﻿using AutoMapper;
using Delivery.BLL.DTO;
using Delivery.BLL.Services;
using Delivery.Web.Controllers;
using Delivery.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Delivery.Web.Tests.Controllers
{
    /// <summary>
    /// Summary description for AdminControllerTests
    /// </summary>
    [TestClass]
    public class AdminControllerTests
    {
        #region Private Members

        private Mock<IAdminService> mockAdminService;

        private Mock<IInvoicesService> mockInvoicesService;

        private string errorMessage;

        private AdminController adminController;

        private ViewResult result;

        #endregion

        #region Utilities

        [TestInitialize]
        public void TestInit()
        {
            mockAdminService = new Mock<IAdminService>();

            mockInvoicesService = new Mock<IInvoicesService>();

            errorMessage = "";

            adminController = new AdminController(mockAdminService.Object, mockInvoicesService.Object);

            result = null;
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            adminController = null;
        }

        #endregion

        #region Tests

        [TestMethod]
        public void DeleteUser_ShouldReturn_PageWithAppUserViewModel()
        {
            // Arrange
            string userId = "";
            var userDto = new AppUserDto { Id = userId };
            mockAdminService.Setup(u => u.GetUserById(userId)).Returns(userDto);
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<AppUserDto, AppUserViewModel>(); });
            adminController.Mapper = config.CreateMapper();

            try
            {
                // Act 
                result = adminController.DeleteUser(userId) as ViewResult;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }

            // Assert
            Assert.IsNotNull(result, errorMessage);
            Assert.IsInstanceOfType(result, typeof(ViewResult), errorMessage);
            Assert.IsNotNull(result.ViewData.Model, errorMessage);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(AppUserViewModel), errorMessage);
            Assert.IsTrue(result.ViewName == "DeleteUser", errorMessage);
            mockAdminService.Verify(u => u.GetUserById(userId));
        }

        [TestMethod]
        public void Invoices_ShouldReturn_ViewAndListOfInvoices()
        {
            // Arrange
            mockInvoicesService.Setup(i => i.GetAll()).Returns(GetTestInvoices());
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<IEnumerable<InvoiceDto>, List<InvoiceViewModel>>(); });
            adminController.Mapper = config.CreateMapper();

            try
            {
                // Act 
                result = adminController.Invoices() as ViewResult;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }

            // Assert
            Assert.IsNotNull(result, errorMessage);
            Assert.IsInstanceOfType(result, typeof(ViewResult), errorMessage);
            Assert.IsNotNull(result.ViewData.Model, errorMessage);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(List<InvoiceViewModel>), errorMessage);
            Assert.IsTrue(result.ViewName == "Invoices", errorMessage);
            mockInvoicesService.Verify(i => i.GetAll());
        }

        [TestMethod]
        public void UserDetails_ShouldReturn_ViewAndDetailInfo()
        {
            // Arrange
            string userId = "asd1";
            var userDto = new AppUserDto { Id = userId };
            mockAdminService.Setup(u => u.GetUserById(userId)).Returns(userDto);
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<AppUserDto, AppUserViewModel>(); });
            adminController.Mapper = config.CreateMapper();

            try
            {
                // Act 
                result = adminController.UserDetails(userId) as ViewResult;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }

            // Assert
            Assert.IsNotNull(result, errorMessage);
            Assert.IsInstanceOfType(result, typeof(ViewResult), errorMessage);
            Assert.IsNotNull(result.ViewData.Model, errorMessage);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(AppUserViewModel), errorMessage);
            Assert.IsTrue(result.ViewName == "UserDetails", errorMessage);
            mockAdminService.Verify(u => u.GetUserById(userId));
        }

        [TestMethod]
        public void Users_ShouldReturn_View()
        {
            // Arrange
            mockAdminService.Setup(u => u.GetUsers()).Returns(GetTestUsers());
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<IEnumerable<AppUserDto>, List<AppUserViewModel>>(); });
            adminController.Mapper = config.CreateMapper();

            try
            {
                // Act 
                result = adminController.Users() as ViewResult;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }

            // Assert
            Assert.IsNotNull(result, errorMessage);
            Assert.IsInstanceOfType(result, typeof(ViewResult), errorMessage);
            Assert.IsNotNull(result.ViewData.Model, errorMessage);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(List<AppUserViewModel>), errorMessage);
            Assert.IsTrue(result.ViewName == "Users", errorMessage);
            mockAdminService.Verify(u => u.GetUsers());
        }

        #endregion

        #region Helpers

        private IEnumerable<AppUserDto> GetTestUsers()
        {
            return new List<AppUserDto> { new AppUserDto { }, new AppUserDto { } };
        }

        private IEnumerable<InvoiceDto> GetTestInvoices()
        {
            var invoicesDtos = new List<InvoiceDto>
            {
                new InvoiceDto
                {
                    Id= 1,
                    PostOperatorName = "",
                    Number="1234567890123",
                    SendDateTime = DateTime.Parse("2020.06.01"),
                    Sender = "Sender 1",
                    SenderAddress = "Sender 1 address",
                    Recipient = "Recipient 1",
                    RecipientAddress = "Recipient 1 Address",
                    CurrentLocation = "Current location 1",
                    ActualStatus = "Actual status 1"
                },
                new InvoiceDto
                {
                    Id= 2,
                    PostOperatorName = "",
                    Number="1234567890124",
                    SendDateTime = DateTime.Parse("2020.06.02"),
                    Sender = "Sender 2",
                    SenderAddress = "Sender 2 address",
                    Recipient = "Recipient 1",
                    RecipientAddress = "Recipient 1 Address",
                    CurrentLocation = "Current location 1",
                    ActualStatus = "Actual status 2"
                }
            };

            return invoicesDtos;
        }

        #endregion
    }
}
