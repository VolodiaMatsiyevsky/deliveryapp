﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Delivery.BLL.DTO;
using Delivery.BLL.Services;
using Delivery.Web.Controllers;
using Delivery.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Delivery.Web.Tests.Controllers
{
    [TestClass]
    public class MainControllerTests
    {
        #region Private Members

        private Mock<IPostOperatorService> mockPostOperatorService;

        private Mock<IInvoicesService> mockInvoicesService;

        private string errorMessage;

        private MainController mainController;

        private ViewResult result;

        #endregion

        #region Utilities

        [TestInitialize]
        public void TestInit()
        {
            mockPostOperatorService = new Mock<IPostOperatorService>();

            mockInvoicesService = new Mock<IInvoicesService>();

            errorMessage = "";

            mainController = new MainController(mockPostOperatorService.Object, mockInvoicesService.Object);

            result = null;
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            mainController = null;
        }

        #endregion

        #region Tests

        [TestMethod]
        public void Index_ShouldReturn_ViewAndNotEmptyModel()
        {
            // Arrange
            var postOperators = new List<PostOperatorDto>
            {
                new PostOperatorDto{ Id=1, Name = "Нова Пошта", LinkToSearchPage= "https://novaposhta.ua/tracking", PathToLogoImage="", IsActive=true},
                new PostOperatorDto{ Id=1, Name = "Укрпошта", LinkToSearchPage= "https://track.ukrposhta.ua/tracking_UA.html", PathToLogoImage="", IsActive=true}
            };
            mockPostOperatorService.Setup(pos => pos.GetAll()).Returns(postOperators);

            try
            {
                // Act
                result = mainController.Index() as ViewResult;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }
            // Assert
            Assert.IsNotNull(result, errorMessage);
        }

        [TestMethod]
        public void Index_Post_CorrectNumber_ShouldReturn_ViewAndNotEmtyInvoice()
        {
            // Arrange
            InvoiceDto invoiceDto = new InvoiceDto { };
            mockInvoicesService.Setup(s => s.SearchByNumber("123456")).ReturnsAsync(invoiceDto);

            InvoiceViewModel invoiceViewModel = null;
            try
            {
                // Act
                result = mainController.Index(new MainIndexViewModel { Number = "123456" }) as ViewResult;
                invoiceViewModel = (InvoiceViewModel)result.Model;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }

            // Assert
            Assert.IsTrue(result.ViewName == "Details", errorMessage);
            Assert.IsNotNull(invoiceViewModel, errorMessage);
        }

        [TestMethod]
        public void Index_Post_UnCorrectNumber_ShouldReturn_DeliveryMessage()
        {
            // Arrange
            InvoiceDto invoiceDto = null;
            mockInvoicesService.Setup(s => s.SearchByNumber("")).ReturnsAsync(invoiceDto);
            DeliveryMessage deliveryMessage = null;

            try
            {
                // Act
                result = mainController.Index(new MainIndexViewModel { Number = "" }) as ViewResult;
                deliveryMessage = (DeliveryMessage)result.Model;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }

            // Assert
            Assert.IsTrue(result.ViewName == "DeliveryMessage", errorMessage);
            Assert.IsNotNull(deliveryMessage, errorMessage);
        }

        #endregion    

    }
}
