﻿using AutoMapper;
using Delivery.BLL.DTO;
using Delivery.BLL.Services;
using Delivery.Web.Controllers;
using Delivery.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Delivery.Web.Tests.Controllers
{
    [TestClass]
    public class PostOperatorControllerTests
    {
        #region Private Members

        private Mock<IPostOperatorService> mockPostOperatorService;

        private string errorMessage;

        private PostOperatorController operatorController;

        private ViewResult result;

        #endregion

        #region Utilities

        [TestInitialize]
        public void TestInit()
        {
            mockPostOperatorService = new Mock<IPostOperatorService>();

            operatorController = new PostOperatorController(mockPostOperatorService.Object);

            errorMessage = "";

            result = null;
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            operatorController = null;
        }

        #endregion

        #region Tests

        [TestMethod]
        public void Create_Returns_RedirectToAction()
        {
            // Arrange
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<PostOperatorViewModel, PostOperatorDto>(); });
            operatorController.Mapper = config.CreateMapper();
            RedirectToRouteResult redirectToRoute = null;

            try
            {
                // Act 
                redirectToRoute = operatorController.Create(GetTestOperatorVM()) as RedirectToRouteResult;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }

            // Assert
            Assert.IsNotNull(redirectToRoute, errorMessage);
            Assert.IsInstanceOfType(redirectToRoute, typeof(RedirectToRouteResult), errorMessage);
            Assert.AreEqual(redirectToRoute.RouteValues["action"], "Index", errorMessage);
        }

        [TestMethod]
        public void Edit_Get_Returns_ViewWithListOfOperators()
        {
            // Arrange
            int id = 1;
            mockPostOperatorService.Setup(po => po.GetById(id)).Returns(GetTestOperatorDto(id));
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<PostOperatorDto, PostOperatorViewModel>(); });
            operatorController.Mapper = config.CreateMapper();

            try
            {
                // Act 
                result = operatorController.Edit(id) as ViewResult;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }

            // Assert
            Assert.IsNotNull(result, errorMessage);
            Assert.IsInstanceOfType(result, typeof(ViewResult), errorMessage);
            Assert.IsNotNull(result.ViewData.Model, errorMessage);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(PostOperatorViewModel), errorMessage);
            Assert.IsTrue(result.ViewName == "Edit", errorMessage);
            mockPostOperatorService.Verify(po => po.GetById(id));
        }

        [TestMethod]
        public void Edit_Post_Returns_RedirectToAction()
        {
            // Arrange
            var postOperator = GetTestOperatorVM();
            postOperator.Id = 1;
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<PostOperatorViewModel, PostOperatorDto>(); });
            operatorController.Mapper = config.CreateMapper();
            RedirectToRouteResult redirectResult = null;

            try
            {
                // Act 
                redirectResult = operatorController.Edit(postOperator) as RedirectToRouteResult;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }

            // Assert
            Assert.IsNotNull(redirectResult, errorMessage);
            Assert.IsInstanceOfType(redirectResult, typeof(RedirectToRouteResult), errorMessage);
            Assert.AreEqual(redirectResult.RouteValues["action"], "Index", errorMessage);
        }

        [TestMethod]
        public void Index_Returns_ViewAndListOfPostOperators()
        {
            // Arrange
            mockPostOperatorService.Setup(po => po.GetAll()).Returns(GetTestPostOperators());
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<IEnumerable<PostOperatorDto>, List<PostOperatorViewModel>>(); });
            operatorController.Mapper = config.CreateMapper();

            try
            {
                // Act 
                result = operatorController.Index() as ViewResult;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }

            // Assert
            Assert.IsNotNull(result, errorMessage);
            Assert.IsInstanceOfType(result, typeof(ViewResult), errorMessage);
            Assert.IsNotNull(result.ViewData.Model, errorMessage);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(List<PostOperatorViewModel>), errorMessage);
            Assert.IsTrue(result.ViewName == "Index", errorMessage);
            mockPostOperatorService.Verify(po => po.GetAll());
        }

        #endregion

        #region Helpers

        private PostOperatorDto GetTestOperatorDto(int id)
        {
            return new PostOperatorDto
            {
                Id = id,
                Name = "New Post",
                LinkToSearchPage = "link1",
                PathToLogoImage = "path1",
                IsActive = true,
                Notes = "notes 1"
            };
        }

        private PostOperatorViewModel GetTestOperatorVM()
        {
            return new PostOperatorViewModel
            {
                Name = "Federal Express",
                LinkToSearchPage = "link3",
                PathToLogoImage = "path3",
                IsActive = true,
                Notes = "notes 3"
            };
        }

        private IEnumerable<PostOperatorDto> GetTestPostOperators()
        {
            var postOperatorsDtos = new List<PostOperatorDto>
            {
                new PostOperatorDto
                {
                    Id= 1,
                    Name = "New Post",
                    LinkToSearchPage = "link1",
                    PathToLogoImage="path1",
                    IsActive = true,
                    Notes = "notes 1"
                },
                new PostOperatorDto
                {
                    Id= 2,
                    Name = "UkrPost",
                    LinkToSearchPage = "link2",
                    PathToLogoImage="path2",
                    IsActive = true,
                    Notes = "notes 3"
                }
            };

            return postOperatorsDtos;
        }

        #endregion
    }
}
