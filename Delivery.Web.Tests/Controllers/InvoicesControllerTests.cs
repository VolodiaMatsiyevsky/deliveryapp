﻿using AutoMapper;
using Delivery.BLL.DTO;
using Delivery.BLL.Services;
using Delivery.Web.Controllers;
using Delivery.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Delivery.Web.Tests.Controllers
{
    [TestClass]
    public class InvoicesControllerTests
    {
        #region Private Members

        private Mock<IInvoicesService> mockInvoicesService;

        private string errorMessage;

        private InvoicesController invoiceController;

        private ViewResult result;

        #endregion

        #region Utilities

        [TestInitialize]
        public void TestInit()
        {
            mockInvoicesService = new Mock<IInvoicesService>();

            var config = new MapperConfiguration(cfg => { });

            errorMessage = "";

            invoiceController = new InvoicesController(mockInvoicesService.Object);

            result = null;
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            invoiceController = null;
        }

        #endregion

        #region Tests

        [TestMethod]
        public void Delete_ShouldReturn_ViewWithInvoiceViewModel()
        {
            // Arrange
            int id = 1;
            mockInvoicesService.Setup(i => i.GetById(id)).Returns(GetTestInvoice(id));
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<InvoiceDto, InvoiceViewModel>(); });
            invoiceController.Mapper = config.CreateMapper();

            try
            {
                // Act 
                result = invoiceController.Delete(id) as ViewResult;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }

            // Assert
            Assert.IsNotNull(result, errorMessage);
            Assert.IsInstanceOfType(result, typeof(ViewResult), errorMessage);
            Assert.IsNotNull(result.ViewData.Model, errorMessage);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(InvoiceViewModel), errorMessage);
            Assert.IsTrue(result.ViewName == "Delete", errorMessage);
            mockInvoicesService.Verify(i => i.GetById(id));
        }

        [TestMethod]
        public void Details_ShouldReturn_ViewAndDetailInfo()
        {
            // Arrange
            int id = 1;
            mockInvoicesService.Setup(i => i.GetById(id)).Returns(GetTestInvoice(id));
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<InvoiceDto, InvoiceViewModel>(); });
            invoiceController.Mapper = config.CreateMapper();

            try
            {
                // Act 
                result = invoiceController.Details(id) as ViewResult;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }

            // Assert
            Assert.IsNotNull(result, errorMessage);
            Assert.IsInstanceOfType(result, typeof(ViewResult), errorMessage);
            Assert.IsNotNull(result.ViewData.Model, errorMessage);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(InvoiceViewModel), errorMessage);
            Assert.IsTrue(result.ViewName == "Details", errorMessage);
            mockInvoicesService.Verify(i => i.GetById(id));
        }

        [TestMethod]
        public void Update_ShouldReturn_Success()
        {
            // Arrange
            int id = 1;
            mockInvoicesService.Setup(i => i.GetById(id)).Returns(GetTestInvoice(id));
            
            RedirectToRouteResult redirectTo = null;

            try
            {
                // Act 
                redirectTo =  invoiceController.UpdateStatus(id) as RedirectToRouteResult;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message + " | " + ex.StackTrace;
            }
            // Assert
            Assert.IsNotNull(redirectTo, errorMessage);
            Assert.IsInstanceOfType(redirectTo, typeof(RedirectToRouteResult), errorMessage);
            Assert.AreEqual(redirectTo.RouteValues["action"], "Details", errorMessage);
            mockInvoicesService.Verify(i => i.GetById(id));
        }

        #endregion

        #region Helpers

        private IEnumerable<InvoiceDto> GetTestInvoices()
        {
            var invoicesDtos = new List<InvoiceDto>
            {
                new InvoiceDto
                {
                    Id= 1,
                    PostOperatorName = "",
                    Number="1234567890123",
                    SendDateTime = DateTime.Parse("2020.06.01"),
                    Sender = "Sender 1",
                    SenderAddress = "Sender 1 address",
                    Recipient = "Recipient 1",
                    RecipientAddress = "Recipient 1 Address",
                    CurrentLocation = "Current location 1",
                    ActualStatus = "Actual status 1"
                },
                new InvoiceDto
                {
                    Id= 2,
                    PostOperatorName = "",
                    Number="1234567890124",
                    SendDateTime = DateTime.Parse("2020.06.02"),
                    Sender = "Sender 2",
                    SenderAddress = "Sender 2 address",
                    Recipient = "Recipient 1",
                    RecipientAddress = "Recipient 1 Address",
                    CurrentLocation = "Current location 1",
                    ActualStatus = "Actual status 2"
                }
            };

            return invoicesDtos;
        }

        private InvoiceDto GetTestInvoice(int id)
        {
            var invoiceDto = new InvoiceDto
            {
                Id = id,
                PostOperatorName = "",
                Number = "1234567890123",
                SendDateTime = DateTime.Parse("2020.06.01"),
                Sender = "Sender 1",
                SenderAddress = "Sender 1 address",
                Recipient = "Recipient 1",
                RecipientAddress = "Recipient 1 Address",
                CurrentLocation = "Current location 1",
                ActualStatus = "Actual status 1",
            };

            return invoiceDto;
        }

        #endregion
    }
}