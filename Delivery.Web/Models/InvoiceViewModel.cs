﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Delivery.Web.Models
{
    /// <summary>
    /// Shipment ViewModel
    /// </summary>
    public class InvoiceViewModel
    {
        /// <summary>
        /// Shipment Id
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Shipment number
        /// </summary>
        [Required]
        [Display(Name = "Номер відправлення")]
        [StringLength(30, ErrorMessage = "Максимальна довжина номера - 30 символів")]
        [MinLength(6, ErrorMessage = "Мінімальна довжина номера - 6 символів")]
        public string Number { get; set; }

        /// <summary>
        /// Postal operator name
        /// </summary>
        [Required(ErrorMessage = "Не вказано поштового оператора")]
        [Display(Name = "Поштовий оператор")]
        public string PostOperatorName { get; set; }

        /// <summary>
        /// Date of sending according to the information system of the postal operator
        /// </summary>
        [Required]
        [Display(Name = "Дата відправлення")]
        public DateTime SendDateTime { get; set; }

        /// <summary>
        /// The address of the current location of shipment
        /// </summary>
        [Display(Name = "Місцезнаходження")]
        [StringLength(30, ErrorMessage = "Максимальна довжина адреси місцезнаходження - 300 символів")]
        public string CurrentLocation { get; set; }

        /// <summary>
        /// Current shipment status
        /// </summary>
        [Display(Name = "Статус")]
        [StringLength(30, ErrorMessage = "Максимальна довжина статусу відправлення - 300 символів")]
        public string ActualStatus { get; set; }

        /// <summary>
        /// Notes
        /// </summary>
        [Display(Name = "Коментар")]
        [StringLength(30, ErrorMessage = "Максимальна довжина коментаря - 300 символів")]
        public string Notes { get; set; }
    }
}