﻿using System.ComponentModel;

namespace Delivery.Web.Models
{
    /// <summary>
    /// User message class in the Delivery system
    /// </summary>
    public class DeliveryMessage : IDeliveryMessage
    {

        /// <summary>
        /// Message body
        /// </summary>
        [DisplayName("Опис")]
        public string Body { get; set; }

        /// <summary>
        /// Message title
        /// </summary>
        [DisplayName("Помилка")]
        public string Title { get; set; }
    }
}