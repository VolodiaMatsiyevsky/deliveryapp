﻿using Delivery.BLL.Services;
using Delivery.Web.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Delivery.Web.Controllers
{
    /// <summary>
    /// Shipment controller
    /// </summary>
    [Authorize]
    public class InvoicesController : BaseController
    {
        private readonly IInvoicesService _invoicesService;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="invoicesService">Object of the Invoices service</param>
        public InvoicesController(IInvoicesService invoicesService)
        {
            _invoicesService = invoicesService;
        }

        /// <summary>
        /// Returns page for create a new shipment
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            return View("Create");
        }

        /// <summary>
        /// Gets the number from the page of creation of the shipment, checks in the list of existing, searches for new in information systems of postal operators and saves in a DB
        /// </summary>
        /// <param name="model">SearchInvoiceViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SearchInvoiceViewModel model)
        {
            if (ModelState.IsValid)
            {
                _invoicesService.Add(User.Identity.GetUserId(), model.Number);

                return RedirectToAction("Index");
            }

            return View("Create");
        }

        /// <summary>
        /// Returns a page to confirm the deletion of the shipment
        /// </summary>
        /// <param name="id">Shipment Id</param>
        /// <returns>Page to confirm the deletion of the shipment</returns>
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null) throw new Exception("Не обрано відправлення для видалення.");

            var invoiceDto = _invoicesService.GetById((int)id);

            if (invoiceDto == null) throw new Exception("Відправлення не знайдено.");

            return View("Delete", Mapper.Map<InvoiceViewModel>(invoiceDto));
        }

        /// <summary>
        /// Deletes the shipment from the db
        /// </summary>
        /// <param name="id">Shipment Id</param>
        /// <param name="invoice">View model of the shipment</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int id, InvoiceViewModel invoice)
        {
            _invoicesService.Remove(id);

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Shipment details
        /// </summary>
        /// <param name="id">Shipment Id</param>
        /// <returns>Page of the shipment info</returns>
        [HttpGet]
        public ActionResult Details(int id)
        {
            var invoiceDto = _invoicesService.GetById(id);

            if (invoiceDto == null) throw new Exception("Відправлення не знайдено.");

            return View("Details", Mapper.Map<InvoiceViewModel>(invoiceDto));
        }

        /// <summary>
        /// Returns page with list of user's shipments
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            string userId = User.Identity.GetUserId();

            return View("Index", Mapper.Map<List<InvoiceViewModel>>(_invoicesService.GetInvoicesByUserId(userId)));
        }

        /// <summary>
        /// Updates shipment status
        /// </summary>
        /// <param name="id">Shipment Id</param>
        /// <returns>Redirects to page of the shipment info</returns>
        [HttpPost]
        public ActionResult UpdateStatus(int id)
        {
            var invoiceDto = _invoicesService.GetById(id);

            if (invoiceDto == null) throw new Exception("Відправлення не знайдено.");

            _invoicesService.UpdateStatusAsync(id);

            return RedirectToAction("Details", id);
        }
    }
}
