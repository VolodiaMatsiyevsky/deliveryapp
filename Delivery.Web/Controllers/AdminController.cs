﻿using Delivery.BLL.DTO;
using Delivery.BLL.Services;
using Delivery.Web.Models;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Delivery.Web.Controllers
{
    /// <summary>
    /// Admin controller
    /// </summary>
    public class AdminController : BaseController
    {
        #region Private Properties

        private readonly IAdminService _adminService;

        private readonly IInvoicesService _invoicesService;

        private IAuthenticationManager _authenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        #endregion

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="adminService">Object of the aadmin service</param>
        /// <param name="invoicesService">Object of the Invoices service</param>
        public AdminController(IAdminService adminService, IInvoicesService invoicesService)
        {
            _adminService = adminService;
            _invoicesService = invoicesService;
        }

        /// <summary>
        /// Main page of Admin
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View("Admin");
        }

        /// <summary>
        /// Returns list of all shipments for all users
        /// </summary>
        /// <returns>Список усіх відправлень</returns>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Invoices()
        {
            return View("Invoices", Mapper.Map<List<InvoiceViewModel>>(_invoicesService.GetAll()));
        }

        #region ManageUsers

        /// <summary>
        /// Returns the list of registered users
        /// </summary>
        /// <returns>List of users</returns>
        public ActionResult Users()
        {
            return View("Users", Mapper.Map<List<AppUserViewModel>>(_adminService.GetUsers()));
        }

        /// <summary>
        /// Users details
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>Page of user info</returns>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult UserDetails(string userId)
        {
            return View("UserDetails", Mapper.Map<AppUserViewModel>(_adminService.GetUserById(userId)));
        }

        /// <summary>
        /// Returns confirmation for deleting the user
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteUser(string userId)
        {
            return View("DeleteUser", Mapper.Map<AppUserViewModel>(_adminService.GetUserById(userId)));
        }

        /// <summary>
        /// Deleting the user from db
        /// </summary>
        /// <param name="appUser">View model of the user</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteUser(AppUserViewModel appUser)
        {
            _invoicesService.RemoveByUser(appUser.Id);

            _adminService.RemoveUser(appUser.Id);

            return RedirectToAction("Users");
        }

        #endregion

        #region Log in @ Registration

        /// <summary>
        /// Returns Login page
        /// </summary>
        /// <param name="returnUrl">The page from which the user applied for logging in</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            return View("Login");
        }

        /// <summary>
        /// PGets data from the user's login page
        /// </summary>
        /// <param name="model">View model of the login</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                ClaimsIdentity claim = await _adminService.Authenticate(Mapper.Map<AppUserDto>(model));
                if (claim == null)
                {
                    ModelState.AddModelError("", "Невірний логін або пароль.");
                }
                else
                {
                    _authenticationManager.SignOut();

                    AuthenticationProperties authPropertiess = new AuthenticationProperties()
                    {
                        IsPersistent = false,
                        ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(30)
                    };

                    if (model.RememberMe)
                    {
                        authPropertiess.IsPersistent = true;
                        authPropertiess.ExpiresUtc = DateTimeOffset.UtcNow.AddDays(14);
                    }

                    _authenticationManager.SignIn(authPropertiess, claim);

                    return RedirectToAction("Index", "Main");
                }
            }

            return View(model);
        }

        /// <summary>
        /// Logout from the system
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogOff()
        {
            _authenticationManager.SignOut();
            return RedirectToAction("Index", "Main");
        }

        /// <summary>
        /// Returns the page for user registration
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View("Register");
        }

        /// <summary>
        /// Gets data from the user registration page, adds a default role, creates default user settings
        /// </summary>
        /// <param name="model">View model of the registration</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _adminService.AddUser(Mapper.Map<AppUserDto>(model));
                await Login(new LoginViewModel { Email = model.Email, Password = model.Password });
            }
            return View(model);
        }

        #endregion
    }
}