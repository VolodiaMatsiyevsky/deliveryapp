﻿using Delivery.BLL.Services;
using Delivery.Web.Models;
using System;
using System.Web.Mvc;

namespace Delivery.Web.Controllers
{
    /// <summary>
    /// Main controller, start page
    /// </summary>
    public class MainController : BaseController
    {
        #region Private Properties

        private readonly IPostOperatorService _postOperatorsService;

        private readonly IInvoicesService _invoicesService;

        #endregion

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="deliveryMessage">Instance of the users message</param>
        /// <param name="postOperatorsService">Object of the PostOperators service</param>
        /// <param name="invoicesService">>Object of the Invoices service</param>
        /// <param name="mapper">Object  map</param>
        public MainController(IPostOperatorService postOperatorsService, IInvoicesService invoicesService)
        {
            _postOperatorsService = postOperatorsService;
            _invoicesService = invoicesService;
        }

        /// <summary>
        /// Returns main page with the form for search posting by the number and the list of post operators 
        /// </summary>
        /// <returns>Main page</returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            var mainIndexViewModel = new MainIndexViewModel
            {
                Number = "",
                PostOperators = _postOperatorsService.GetAll()
            };

            return View("Index", mainIndexViewModel);
        }

        /// <summary>
        /// Searchs posting in accounting systems of post operators
        /// </summary>
        /// <param name="model">View model of the posting</param>
        /// <returns>Page with posting info</returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(MainIndexViewModel model)
        {
            if (model.Number.Length < 6 || model.Number.Length > 30) throw new Exception("Введіть номер від 6 до 30 символів.");

            var invoiceDto = _invoicesService.SearchByNumber(model.Number);

            if (invoiceDto == null) throw new Exception("Відправлення не знайдено.");

            return View("Details", Mapper.Map<InvoiceViewModel>(invoiceDto));
        }

        [HttpGet]
        public ActionResult ShowError(string url, string message)
        {
            var deliveryMessage = new DeliveryMessage
            {
                Title = string.IsNullOrEmpty(url) ? "Error" : url,
                Body = string.IsNullOrEmpty(message) ? "Unknown error" : message
            };

            return View("ShowError", deliveryMessage);
        }
    }
}