﻿using Delivery.BLL.DTO;
using Delivery.BLL.Services;
using Delivery.Web.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Delivery.Web.Controllers
{
    /// <summary>
    /// PostOperators controller
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class PostOperatorController : BaseController
    {
        private readonly IPostOperatorService _postOperatorService;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="postOperatorService">Object of the PostOperators service</param>
        public PostOperatorController(IPostOperatorService postOperatorService)
        {
            _postOperatorService = postOperatorService;
        }

        /// <summary>
        /// Returns page for creating the new PostOperator
        /// </summary>
        /// <returns>Page for creating the new PostOperator</returns>
        [HttpGet]
        public ActionResult Create()
        {
            return View("Create");
        }

        /// <summary>
        /// Creates the new PostOperator
        /// </summary>
        /// <param name="postOperator">ViewModel of the PostOperator</param>
        /// <returns>View for creating of the PostOperator</returns>
        [HttpPost]
        public ActionResult Create(PostOperatorViewModel postOperator)
        {
            if (ModelState.IsValid)
            {
                _postOperatorService.Add(Mapper.Map<PostOperatorDto>(postOperator));

                return RedirectToAction("Index");
            }

            return View("Create", postOperator);
        }

        /// <summary>
        /// Returns page for edit PostOperator. Admin can edit only status.
        /// </summary>
        /// <param name="id">PostOperator's Id</param>
        /// <returns>View Edit</returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null) throw new Exception("Не обрано поштового оператора для оновлення активності.");

            var postOperatorDto = _postOperatorService.GetById((int)id);

            if (postOperatorDto == null) throw new Exception("Поштового оператора не знайдено.");

            return View("Edit", Mapper.Map<PostOperatorViewModel>(postOperatorDto));
        }

        /// <summary>
        /// Saves edited data to database
        /// </summary>
        /// <param name="postOperator">ViewModel of the PostOperator</param>
        /// <returns>redirect to Index-page</returns>
        [HttpPost]
        public ActionResult Edit(PostOperatorViewModel postOperator)
        {
            if (ModelState.IsValid)
            {
                _postOperatorService.UpdatePostOperator(Mapper.Map<PostOperatorDto>(postOperator));

                return RedirectToAction("Index");
            }

            return View("Edit", postOperator);
        }

        /// <summary>
        /// Returns page with the list of PostOperators
        /// </summary>
        /// <returns>page with the list of PostOperators</returns>
        public ActionResult Index()
        {
            return View("Index", Mapper.Map<List<PostOperatorViewModel>>(_postOperatorService.GetAll()));
        }
    }
}
