﻿using AutoMapper;
using Delivery.Web.Models;
using System.Web.Mvc;

namespace Delivery.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        //protected internal IMapper Mapper { get; }
        public IMapper Mapper { get; set; }

        protected internal IDeliveryMessage DeliveryMessage { get; set; } = new DeliveryMessage();

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.Result = RedirectToAction("ShowError", "Main", new { url = Request.Url, message = filterContext.Exception.Message });
        }
    }
}