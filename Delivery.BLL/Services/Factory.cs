﻿using AutoMapper;
using Delivery.BLL.DTO;
using Delivery.DAL.Models;
using System.Collections.Generic;

namespace Delivery.BLL.Services
{
    /// <summary>
    /// A class for creating search agents by the factory method, contains singleton for IMapper
    /// </summary>
    public class Factory
    {
        private static IMapper mapper;

        /// <summary>
        /// Returns a list of search agents implemented in the Delivery system
        /// </summary>
        /// <param name="apiKeys">Access keys to implemented Api-services</param>
        /// <returns>List of search agents</returns>
        public static IEnumerable<ISearchAgent> GetAllAgents(Dictionary<string, string> apiKeys)
        {
            var listOfAgents = new List<ISearchAgent>
            {
                new ApiSearcherAgent(apiKeys["ApiKeyNovaPoshta"]),
                new HtmlSearcherAgent()
            };

            return listOfAgents;
        }

        /// <summary>
        /// Singleton of IMapper
        /// </summary>
        /// <returns></returns>
        public static IMapper GetMapper()
        {
            if (mapper == null) mapper = Configure();

            return mapper;
        }

        private static IMapper Configure()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Invoice, InvoiceDto>();
                cfg.CreateMap<InvoiceDto, Invoice>();
                cfg.CreateMap<PostOperatorDto, PostOperator>();
                cfg.CreateMap<PostOperator, PostOperatorDto>();
            });

            return config.CreateMapper();
        }
    }
}
