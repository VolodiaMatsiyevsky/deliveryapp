﻿using Delivery.DAL.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Delivery.DAL.EF
{
    /// <summary>
    /// DbContext
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="conectionString"></param>
        public ApplicationDbContext(string conectionString) : base(conectionString)
        {
            Configuration.LazyLoadingEnabled = true;
        }

        public virtual DbSet<PostOperator> PostOperators { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }
    }
}
