﻿using System.ComponentModel.DataAnnotations;

namespace Delivery.DAL.Models
{
    /// <summary>
    /// Postal operator
    /// </summary>
    public class PostOperator : IPostOperator
    {
        /// <summary>
        /// Postal operator Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Postal operator name
        /// </summary>
        [Required]
        [Display(Name = "Назва")]
        [StringLength(50, ErrorMessage = "Максимальна довжина назви - 50 символів")]
        [MinLength(6, ErrorMessage = "Мінімальна довжина назви - 6 символів")]
        public string Name { get; set; }

        /// <summary>
        /// Search page for tracking shipments by number in the information system of the postal operator.
        /// </summary>
        [Required]
        [Display(Name = "Посилання для пошуку відправлень")]
        [StringLength(50, ErrorMessage = "Максимальна довжина посилання - 200 символів")]
        [MinLength(6, ErrorMessage = "Мінімальна довжина назви - 10 символів")]
        public string LinkToSearchPage { get; set; }

        /// <summary>
        /// The path to the location of the image of the postal operator's logo on the host server of the Delivery service.
        /// </summary>
        [StringLength(50, ErrorMessage = "Максимальна довжина шляху - 200 символів")]
        public string PathToLogoImage { get; set; }

        /// <summary>
        /// Indicates whether the operator is available for use. The administrator can deactivate the operator in case of changes in access conditions that have not yet been implemented in the Delivery system.
        /// </summary>
        [Required]
        [Display(Name = "Активний?")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Notes
        /// </summary>
        [Display(Name = "Коментар")]
        [StringLength(50, ErrorMessage = "Максимальна довжина коментаря - 200 символів")]
        public string Notes { get; set; }
    }
}
