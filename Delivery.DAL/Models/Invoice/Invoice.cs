﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Delivery.DAL.Models
{
    /// <summary>
    /// User-tracked shipment. It is assumed that it exists in the information system of the postal operator
    /// </summary>
    public class Invoice : IInvoice
    {
        /// <summary>
        /// Shipment Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// User Id.
        /// </summary>
        [Required]
        public string AccountUserId { get; set; }

        /// <summary>
        /// Postal operator Id.
        /// </summary>
        [Required]
        public int PostOperatorId { get; set; }

        /// <summary>
        /// Shipment number in the information system of one of the postal operators.
        /// </summary>
        [Required]
        [Display(Name = "Номер відправлення")]
        [StringLength(30, ErrorMessage = "Максимальна довжина номера - 30 символів")]
        [MinLength(6, ErrorMessage = "Мінімальна довжина номера - 6 символів")]
        public string Number { get; set; }

        /// <summary>
        /// Date of sending according to the information system of the postal operator.
        /// </summary>
        [Required]
        [Display(Name = "Дата відправлення")]
        public DateTime SendDateTime { get; set; }

        /// <summary>
        /// Sender name
        /// </summary>
        [Display(Name = "Відправник")]
        [StringLength(30, ErrorMessage = "Максимальна довжина назви відправника - 128 символів")]
        public string Sender { get; set; }

        /// <summary>
        /// Sender address
        /// </summary>
        [Display(Name = "Адреса відправника")]
        [StringLength(30, ErrorMessage = "Максимальна довжина адреси відправника - 300 символів")]
        public string SenderAddress { get; set; }

        /// <summary>
        /// Recipient name
        /// </summary>
        [Display(Name = "Одержувач")]
        [StringLength(30, ErrorMessage = "Максимальна довжина назви одержувача - 128 символів")]
        public string Recipient { get; set; }

        /// <summary>
        /// Recipient address
        /// </summary>
        [Display(Name = "Адреса одержувача")]
        [StringLength(30, ErrorMessage = "Максимальна довжина адреси одержувача - 300 символів")]
        public string RecipientAddress { get; set; }

        /// <summary>
        /// The address of the current location of shipment
        /// </summary>
        [Display(Name = "Місцезнаходження")]
        [StringLength(30, ErrorMessage = "Максимальна довжина адреси місцезнаходження - 300 символів")]
        public string CurrentLocation { get; set; }

        /// <summary>
        /// Shipment current status
        /// </summary>
        [Display(Name = "Статус")]
        [StringLength(30, ErrorMessage = "Максимальна довжина статусу відправлення - 300 символів")]
        public string ActualStatus { get; set; }

        /// <summary>
        /// Notes
        /// </summary>
        [Display(Name = "Коментар")]
        [StringLength(30, ErrorMessage = "Максимальна довжина коментаря - 300 символів")]
        public string Notes { get; set; }
    }
}
